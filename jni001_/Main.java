//Archivo: Main.java

//Cargar una libreria nativa.
//Crear un objeto e invocar metodos nativos.

public class Main{
    static{
        System.loadLibrary("LibreriaNativa");

    }//End static

    public static void main(String [] args){
        NativeMethods nm = new NativeMethods();
        nm.nativoUno();
        nm.nativoDos();

    }//End void main

}//End Main

