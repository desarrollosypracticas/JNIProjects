//Archivo: clmplOne.c
//Implementar nativeOne en NativeMethods

#include <stdio.h>
#include "NativeMethods.h"

JNIEXPORT void JNICALL Java_NativeMethods_nativoUno(JNIEnv* env, jobject thisObj){
    printf("Hola Mundo Avanzado Java.\n");

}//End nativeOne