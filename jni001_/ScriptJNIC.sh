JNI_INCLUDE="-I/usr/lib/jvm/java-8-openjdk-amd64/include 
             -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux"

export LD_LIBRARY_PATH=.
echo "========================================="
echo 
javac NativeMethods.java Main.java   
echo 'Archivo(s) Java compilado(s).'
echo
javah -jni NativeMethods
echo 'Archivo cabecera creado.'
echo
gcc $JNI_INCLUDE -shared clmplOne.c clmplTwo.c -o libLibreriaNativa.so
echo 'Libreria compartida creada.'
echo "========================================="
echo
echo ">>> Salida <<<"
java Main
echo "========================================="
echo
