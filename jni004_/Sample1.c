#include "Sample1.h"
#include <string.h>


//JNI jint
JNIEXPORT jint JNICALL Java_Sample1_intMethod(JNIEnv *env, jobject obj, kint num){
    return num*num;

}

//JNI jboolean
JNIEXPORT jboolean JNICALL Java_Sample1_booleanMethod(JNIEnv *env, jobject obj, jboolean boolean){
    return !boolean;
}

//JNI jstring
JNIEXPORT jstring JNICALL Java_Sample1_stringMethod(JNIEnv *env, jobject obj, jstring string){
    const char *str=(*env)->GetStringUTFChars(env, string, 0);
    char cap[128];
    strcpy(cap,str);
    (*env)->ReleaseStringUTFChars(env,string,str);
    return (*env)->NewStringUTF (env,strupr(cap));
}

//JNI jintarray
JNIEXPORT jint JNICALL Java_Sample1_intArrayMethod(JNIEnv *env, jobject obj, jintArray array){
    int i,sum=0;
    jsize len=
}