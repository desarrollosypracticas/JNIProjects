//Archivo: Reverse.java

public class Reverse{
    static{
        System.loadLibrary("Reverse");

    }//End static

    public static native String reverse(String s);

    public static void main(String [] args){
        String s = reverse("Ser o no ser.");
        System.out.println(s);

    }//End void main

}//End class Reverse