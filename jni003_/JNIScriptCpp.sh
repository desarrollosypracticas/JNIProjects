JNI_INCLUDE="-I/usr/lib/jvm/java-8-openjdk-amd64/include
             -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux"

export LD_LIBRARY_PATH=.
echo "========================================"
echo
javac Reverse.java
echo 'Archivo(s) Java compilado(s).'
echo
javah -jni Reverse
echo 'Archivo cabecera creado.'
echo
g++ -Wall $JNI_INCLUDE -shared reverse.cpp -o libReverse.so
echo 'Shared library created'
echo "========================================="
echo 
echo ">>> Salida <<<"
echo
java Reverse 
echo "========================================"