JNI_INCLUDE="-I/usr/lib/jvm/java-8-openjdk-amd64/include
             -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux"
export LD_LIBRARY_PATH=.
echo "======================================"
echo
javac Compute.java
echo "Archivo(s) Java Compilado(s)."
echo
javah -jni Compute
echo "Archivo cabecera creado."
echo
gcc $JNI_INCLUDE -shared cmp.c -o libComputeJNI.so
echo "Libreria compartida creada."
echo "======================================"
echo
echo ">>> Salida <<<"
java Compute
echo "======================================"
echo