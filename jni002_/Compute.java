
public class Compute{
    static native double compute(int m,long n);//End compute

    static{
        System.loadLibrary("ComputeJNI");
    }//End static

    public static void main(String [] args){
        double answer = compute(45, 67L);

        System.out.println("Respuesta: "+answer);

    }//End void main

}//End Compute